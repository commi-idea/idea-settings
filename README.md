# IntelliJ IDEA Settings

These are personal settings, not recommended to use them without some consultancy.

## Quick Guide

- Settings / Plugins / Search = "Settings Repository" > Install > Restart
- File / Manage IDE Settings / Settings Repository > Upstream URL = *link to this repo* > Overwrite Local

1. Settings / Keymap // Keymap = CommiKeymap > Apply
1. Settings / Editor / Color Scheme // Scheme = CommiDarcula > Apply
1. Settings / Tools / Settings Repository // Read-only sources:
    1. https://gitlab.com/commi-idea/idea-codestyle
    1. https://gitlab.com/commi-idea/idea-templates
    1. Apply
1. Settings / Editor / Code Style // Scheme = CommiCodeStyle > Apply

- Note: #3 is broken in IDEA 2023.*

## A Bit Slower Guide

- https://www.jetbrains.com/help/idea/sharing-your-ide-settings.html

Since IDEA 2022.3 JetBrains have deprecated the Settings Repository plugin. Now you need to install it on your own.

## Known Issues

- As of 2020.3.2 fileTemplates are not syncing.
  To make it work copy <b>fileTemplates</b> folder to the root of your IDEA configuration.
    - This is {user}/AppData/Roaming/JetBrains/{idea} by default for Windows.